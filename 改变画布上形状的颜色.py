from tkinter import *
root = Tk()
canvas =Canvas(root,width=200,height=150)
canvas.pack()
id1 = canvas.create_polygon(10,10,10,60,50,35,fill="red")
id2 = canvas.create_rectangle(10,60,70,120,fill="grey")
def changecolor(event):
    if event.keysym == 'g' or event.keysym == 'G':
        canvas.itemconfig(id1,fill="green")
        canvas.itemconfig(id2,fill='green')
    elif event.keysym == 'b' or event.keysym == 'B':
        canvas.itemconfig(id1,fill='blue')
        canvas.itemconfig(id2,fill='blue')
canvas.bind_all('<KeyPress-b>', changecolor)
canvas.bind_all('<KeyPress-g>',changecolor)
canvas.bind_all('<KeyPress-G>',changecolor)
canvas.bind_all('<KeyPress-B>',changecolor)
root.mainloop()