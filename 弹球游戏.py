import tkinter
import random
import time
tk = tkinter.Tk()
tk.title('game')
tk.resizable(0,0)
canvas = tkinter.Canvas(tk,width=500,height=400,bd=0)
canvas.pack()
tk.update()

class Ball():
    def __init__(self,canvas,color,paddle):
        self.paddle = paddle
        self.canvas=canvas
        self.id = self.canvas.create_oval(10,10,25,25,fill=color)
        self.canvas.move(self.id,245,100)
        starts = [-3, -2, -1, 1, 1, 2, 3]
        random.choice(starts)
        self.x = starts[0]
        self.y = -3
        self.hit_bottom=False
    def draw(self):
        self.canvas.move(self.id,self.x,self.y)
        pos = self.canvas.coords(self.id)
        if pos[0] <=0:
            self.x = 1
        if pos[2] >=500:
            self.x = -1
        if pos[1] <=0:
            self.y = 1
        if pos[3] >=400:
            self.y = -1
        if self.hit_paddle(pos) == True:
            self.y = -3
        if pos[3]>= 400:
            self.hit_bottom=True
    def hit_paddle(self, pos):
        paddle_pos = self.canvas.coords(self.paddle.id)
        if pos[2] >= paddle_pos[0] and pos[0] <= paddle_pos[2]:
            if pos[3] >= paddle_pos[1] and pos[1] <= paddle_pos[3]:
                return True
        return False
class Paddle:
    def __init__(self,canvas,color):
        self.canvas = canvas
        self.id = self.canvas.create_rectangle(0,0,150,10,fill=color)
        self.canvas.move(self.id,200,350)
        self.canvas.width = self.canvas.winfo_width()
        self.canvas.bind_all('<KeyPress-Left>',self.turn_left)
        self.canvas.bind_all('<KeyPress-Right>',self.turn_right)
        self.x = 0

    def draw(self):
        self.canvas.move(self.id,self.x,0)
        pos = self.canvas.coords(self.id)
        if pos[0] <= 0:
            self.x = 0
        if pos[2] >= 500:
            self.x = 0
    def turn_left(self,evt):
        self.x = -2
    def turn_right(self,evt):
        self.x = 2




paddle = Paddle(canvas,'blue')
ball = Ball(canvas,'red',paddle)
while 1:
    if ball.hit_bottom==False:
        paddle.draw()
        ball.draw()
        tk.update_idletasks()
        tk.update()
        time.sleep(0.01)
    else:
        break

