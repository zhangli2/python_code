from tkinter import *
root = Tk()
canvas = Canvas(root,width=400,height=500)
canvas.pack()
id = canvas.create_polygon(10,10,100,50,10,100)
def movetriangle(event):
    if event.keysym == 'Up':
        canvas.move(id,0,-3)
    elif event.keysym == 'Down':
        canvas.move(id,0,3)
    elif event.keysym == "Left":
        canvas.move(id,-3,0)
    elif event.keysym == "Right":
        canvas.move(id,3,0)
canvas.bind_all('<KeyPress-Up>',movetriangle)
canvas.bind_all('<KeyPress-Down>',movetriangle)
canvas.bind_all('<KeyPress-Left>',movetriangle)
canvas.bind_all('<KeyPress-Right>',movetriangle)
root.mainloop()