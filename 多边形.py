from tkinter import *
root = Tk()
root.title("my polygon")
canvas = Canvas(root,width=200,height = 100)
canvas.pack()
p1 = 100,10
p2 = 140,30
p3 = 20,80
p4 = 40,90
canvas.create_polygon(p1,p2,p3,p4,fill="yellow",outline="blue")
canvas.create_text(100,10,text="My first shape",font=('Courier',12))
root.mainloop()